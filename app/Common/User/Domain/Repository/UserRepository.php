<?php

namespace Application\Common\User\Domain\Repository;
use Application\Common\User\Domain\Model\User;

interface UserRepository
{

    public function findByEmail(string $email):?User;

    public function save(User $user):User;
}