<?php

namespace Application\Welcome\Domain\Repository;

use Application\Welcome\Domain\Model\Tarea;

interface TareaRepository
{

    public function findTareaById(string $id): ?Tarea;

    public function save(Tarea $tarea): Tarea;

}