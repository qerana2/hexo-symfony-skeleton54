<?php

namespace Application\Welcome\Infrastructure\Controller;

use Application\Common\Infrastructure\Controller\CommonController;
use Application\Welcome\Infrastructure\Storage\Doctrine\Repository\TareaDoctrineRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Response;


class WelcomeController extends CommonController
{

    public function index(): Response
    {
        return new Response('h, i m a welcome page.');

    }

    public function secured(ManagerRegistry $registry): Response
    {

        $tareaRepo = new TareaDoctrineRepository($registry);
        $tarea = $tareaRepo->findTareaById('4632792a-d314-4a2d-8451-afe4e1c8fd65');
        $tarea->changeEstado(2);

        $tareaRepo->save($tarea);
        dd($tarea);

        return new Response('Hi, im a secure page');
    }

}